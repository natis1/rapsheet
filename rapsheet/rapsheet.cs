﻿using Modding;

/* 
 * Just a thing where I try out weird concepts and see what they're like. Feel free to steal any code.
 * 
 * 
 * 
 */


namespace rapsheet
{
    public class RapSheetMod : Mod, ITogglableMod
    {
        public override void Initialize()
        {
            ModHooks.Instance.LanguageGetHook += rapsheetText;
            ModHooks.Instance.AfterSavegameLoadHook += addToGame;
            ModHooks.Instance.NewGameHook += newGame;


        }
        
        private void addToGame(SaveGameData data)
        {
            newGame();
        }

        private void newGame()
        {
            load_assets.loadAllTextures();
            GameManager.instance.gameObject.AddComponent<voidify_concept>();
            GameManager.instance.gameObject.AddComponent<custom_statue_concept>();
        }


        public void Unload()
        {
            ModHooks.Instance.LanguageGetHook -= rapsheetText;
        }

        public override string GetVersion()
        {
            return "420";
        }

        private string rapsheetText(string key, string sheettitle)
        {
            if (sheettitle == "CP3" && key == "GG_SUMMARY_TITLE")
            {
                return "Rap Sheet";
            }
            return Language.Language.GetInternal(key, sheettitle);
        }
    }
}