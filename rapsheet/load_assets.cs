using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

namespace rapsheet
{
    public static class load_assets
    {
        public static Dictionary<string, Texture2D> ALL_TEXTURES = new Dictionary<string, Texture2D>();
        
        public static void loadAllTextures()
        {
            foreach (string res in Assembly.GetExecutingAssembly().GetManifestResourceNames())
            {
                if (res.EndsWith(".png"))
                {
                    log("Adding texture with name " + res);
                    ALL_TEXTURES.Add(res, loadImageFromAssembly(res));
                }
            }

        }
        
        private static Texture2D loadImageFromAssembly(string imageName)
        {
            Stream imageStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(imageName);
            if (imageStream != null)
            {
                byte[] buffer = new byte[imageStream.Length];
                imageStream.Read(buffer, 0, buffer.Length);
                imageStream.Dispose();

                //Create texture from bytes
                Texture2D tex = new Texture2D(1, 1);
                tex.LoadImage(buffer);
                tex.Apply();
                return tex;
            }
            log("Unable to load image for some reason because imageStream was null.");
            return new Texture2D(1, 1);
        }
        
        
        private static void log(string str)
        {
            Modding.Logger.Log("[Rap Sheet] " + str);
        }
    }
}