using System.Collections;
using ModCommon;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace rapsheet
{
    public class custom_statue_concept : MonoBehaviour
    {
        private const string STATUE_ROOM = "GG_Workshop";
        
        private void Start()
        {
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += findStatue;

        }

        private void OnDestroy()
        {
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged -= findStatue;
        }

        private void findStatue(Scene from, Scene to)
        {
            if (to.name == STATUE_ROOM)
            {
                StartCoroutine(loadStatueAfterAFewFrames());
            }
        }

        private IEnumerator loadStatueAfterAFewFrames()
        {
            yield return null;
            yield return null;
            SpriteRenderer sr = GameObject.Find("GG_Statue_HollowKnight").FindGameObjectInChildren("Base")
                .FindGameObjectInChildren("Statue").FindGameObjectInChildren("GG_statues_0006_5")
                .GetComponent<SpriteRenderer>();
            Texture2D tempTex = load_assets.ALL_TEXTURES["rapsheet.hollowknight.png"];
            tempTex.Apply();
            sr.sprite = Sprite.Create(tempTex,
                new Rect(0, 0, tempTex.width, tempTex.height), new Vector2(0.15f, 0.48f), sr.sprite.pixelsPerUnit);
            Modding.Logger.Log("Loaded HK statue because in correct room. I hope!");
        }
    }
}