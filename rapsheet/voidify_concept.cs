using System;
using System.Collections;
using ModCommon;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace rapsheet
{
    public class voidify_concept : MonoBehaviour
    {
        private static readonly Color INFECTED_NEW = new Color(0f, 0.25f, 0.75f);
        
        
        private void Start()
        {
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += startVoidify;

        }

        private void OnDestroy()
        {
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged -= startVoidify;

        }

        private void startVoidify(Scene from, Scene to)
        {
            StartCoroutine(voidify(to));
        }
        
        private IEnumerator voidify(Scene to)
        {
            yield return new WaitForSeconds(0.15f);
            foreach (GameObject go in to.GetRootGameObjects())
            {
                if (go.name.Contains("Dream Plant Orb"))
                {
                    go.GetComponent<SpriteRenderer>().color = INFECTED_NEW;
                    go.FindGameObjectInChildren("Get Particles").GetComponent<ParticleSystemRenderer>().material.color = INFECTED_NEW;
                    go.FindGameObjectInChildren("Trail").GetComponent<ParticleSystemRenderer>().material.color = INFECTED_NEW;
                    go.FindGameObjectInChildren("Activate Particles").GetComponent<ParticleSystemRenderer>().material.color = INFECTED_NEW;
                    
                    Modding.Logger.Log("Fixed an infection thingy.");
                } else if (go.name.Contains("Infected Parent"))
                {
                    foreach (Transform child in go.transform)
                    {
                        if (child.name.Contains("infected_vine"))
                        {
                            child.GetComponent<SpriteRenderer>().color = INFECTED_NEW;
                            
                            foreach (Transform secondLevelChild in child.gameObject.transform)
                            {
                                try
                                {
                                    secondLevelChild.gameObject.GetComponent<SpriteRenderer>().color = INFECTED_NEW;
                                    Destroy(secondLevelChild.GetComponent<Animator>());
                                }
                                catch (Exception e)
                                {
                                    Modding.Logger.Log("Unable to change sprite color because " + e);
                                }
                            }
                            Destroy(child.GetComponent<AudioSource>());
                            Destroy(child.GetComponent<InfectedBurstLarge>());
                            
                        } else if (child.name.Contains("infected_floor_goop") || child.name.Contains("infected_large_blob") || child.name.Contains("infected_floor_goop"))
                        {
                            try
                            {
                                child.GetComponent<SpriteRenderer>().color = INFECTED_NEW;
                                Destroy(child.GetComponent<Animator>());
                                
                                foreach (Transform secondChild in child.gameObject.transform)
                                {
                                    Destroy(secondChild);
                                }
                                Destroy(child.GetComponent<InfectedBurstLarge>());
                            }
                            catch (Exception e)
                            {
                                Modding.Logger.Log("Unable to set goop or blob color because " + e);
                            }

                            try
                            {
                                Destroy(child.GetComponent<AudioSource>());

                            }
                            catch (Exception e)
                            {
                                Modding.Logger.Log("Unable to destroy audio source on " + child.name + " probably because it don't exist");
                            }
                        } else if (child.name.Contains("infected_orange_drip"))
                        {
                            Destroy(child.gameObject);
                        } else if (child.name.Contains("infected_crossroads_particles"))
                        {
                            try
                            {
                                foreach (Transform c2 in child.gameObject.transform)
                                {
                                    //ParticleSystem.MainModule m = c2.GetComponent<ParticleSystem>().main;
                                    //m.startColor = new ParticleSystem.MinMaxGradient(INFECTED_NEW);
                                    ParticleSystem.ColorOverLifetimeModule cot = c2.GetComponent<ParticleSystem>()
                                        .colorOverLifetime;                                    
                                    cot.color = new ParticleSystem.MinMaxGradient(INFECTED_NEW);
                                }

                                Modding.Logger.Log("Fixed an infected particle");
                            }
                            catch (Exception e)
                            {
                                Modding.Logger.Log("Unable to set particle system color because " + e);
                            }
                        } else if (child.name.Contains("Pt Mist"))
                        {
                    
                    
                    
                            ParticleSystem.MainModule m = child.GetComponent<ParticleSystem>().main;
                            m.startColor = new ParticleSystem.MinMaxGradient(INFECTED_NEW);
                    
                            ParticleSystem.ColorOverLifetimeModule cot = child.GetComponent<ParticleSystem>()
                                .colorOverLifetime;                                    
                            cot.color = new ParticleSystem.MinMaxGradient(INFECTED_NEW);

                    
                            Modding.Logger.Log("Fixed an infected mist particle");
                        }
                    } 
                } else if (go.name.Contains("_Scenery"))
                {
                    foreach (Transform child in go.transform)
                    {
                        if (child.name.Contains("Water Drip"))
                        {
                            Destroy(child.gameObject);
                        }
                    }
                }
            }
            Modding.Logger.Log("That's all, folks!");
        }

    }
}